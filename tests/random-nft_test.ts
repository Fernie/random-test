
import { Clarinet, Tx, Chain, Account, types } from 'https://deno.land/x/clarinet@v0.14.0/index.ts';
import { assertEquals } from 'https://deno.land/std@0.90.0/testing/asserts.ts';

Clarinet.test({
    name: "Ensure that <...>",
    async fn(chain: Chain, accounts: Map<string, Account>) {
        let deployerWallet = accounts.get("deployer") as Account;
        let wallet1 = accounts.get("wallet_1") as Account;
        let wallet2 = accounts.get("wallet_2") as Account;

        chain.mineEmptyBlockUntil(1);

        let block;
        
        let nums: any[] = [];
        for(let i = 0; i < 300; i++) {
            block = chain.mineBlock([
                Tx.contractCall(
                    `random-nft`,
                    "mint",
                    [],
                    deployerWallet.address
                )
            ]);
        }

        // for(let i = 0; i < 19; i++) {
        //     block = chain.mineBlock([
        //         Tx.contractCall(
        //             `random-nft`,
        //             "mint",
        //             [],
        //             wallet1.address
        //         )
        //     ]);
        // }
        block = chain.mineBlock([
            Tx.contractCall(
                `random-nft`,
                "mint",
                [],
                wallet2.address
            )
        ]);

        let assetMaps = chain.getAssetsMaps();
        console.log(assetMaps);


        block = chain.mineBlock([
            Tx.contractCall(
                `random-nft`,
                "uint-to-string",
                [types.uint(299)],
                wallet2.address
            ),
            Tx.contractCall(
                `random-nft`,
                "uint-to-string",
                [types.uint(0)],
                wallet2.address
            ),
            Tx.contractCall(
                `random-nft`,
                "get-token-uri",
                [types.uint(10)],
                wallet2.address
            ),
        ]);
        console.log(block.receipts[2].result)
    },
});
